/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <string>
#include <iostream>
#include <sstream>

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

#define BUFFER_SIZE 5120
class RemoteConsoleServer
{
public:
    RemoteConsoleServer(){

    }
    ~RemoteConsoleServer()
    {
        stop();
    }

    bool start(const int portNumber)
    {
        _portno = portNumber;

        _sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (_sockfd < 0)
        {
           printf("ERROR opening socket\n");
           return false;
        }

        bzero((char *) &_serv_addr, sizeof(_serv_addr));
        _serv_addr.sin_family = AF_INET;
        _serv_addr.sin_addr.s_addr = INADDR_ANY;
        _serv_addr.sin_port = htons(_portno);
        if (bind(_sockfd, (struct sockaddr *) &_serv_addr, sizeof(_serv_addr)) < 0)
        {
            printf("ERROR on binding\n");
            return false;
        }
        return true;
    }

    void stop()
    {
        std::string exitCmd = "exit";
        int n = write(_newsockfd,exitCmd.c_str(),exitCmd.size());
        if (n < 0)
        {
            printf("ERROR writing to socket\n");
        }

        close(_newsockfd);
        close(_sockfd);
    }

    bool waitConnection()
    {
        listen(_sockfd,1);
        _clilen = sizeof(_cli_addr);
        _newsockfd = accept(_sockfd,
                    (struct sockaddr *) &_cli_addr,
                    &_clilen);
        if (_newsockfd < 0)
        {
             printf("ERROR on accept\n");
             return false;
        }
        return true;
    }

    bool sendCommand( const std::string& cmd, std::string& response )
    {
        std::stringstream ss;

        ss << cmd << " 2>&1";
        std::string fullCmd = ss.str();

        int n = write(_newsockfd,fullCmd.c_str(),fullCmd.size());
        if (n < 0)
        {
            printf("ERROR writing to socket\n");
            return false;
        }
        bzero(_buffer,5120);
        n = read(_newsockfd,_buffer,BUFFER_SIZE);
        if (n < 0)
        {
            printf("ERROR reading from socket\n");
            return false;
        }

        response = _buffer;

        return true;
    }

private:
    int _sockfd, _newsockfd, _portno;
    struct sockaddr_in _serv_addr, _cli_addr;
    socklen_t _clilen;
    char _buffer[BUFFER_SIZE];
};

int main(int argc, char *argv[])
{
    RemoteConsoleServer rcs;

    rcs.start(55555);
    bool connected = rcs.waitConnection();

    if(!connected) return -1;

    std::string str, res;
    while(1)
    {
        std::cin >> str;
        rcs.sendCommand(str, res);
        std::cout << res << std::endl;
    }

    std::cout << "End of the connection" << std::endl;
//     int sockfd, newsockfd, portno;
//     socklen_t clilen;
//     char buffer[256];
//     struct sockaddr_in serv_addr, cli_addr;
//     int n;
//     if (argc < 2) {
//         fprintf(stderr,"ERROR, no port provided\n");
//         exit(1);
//     }
//     sockfd = socket(AF_INET, SOCK_STREAM, 0);
//     if (sockfd < 0)
//        error("ERROR opening socket");

//     bzero((char *) &serv_addr, sizeof(serv_addr));
//     portno = atoi(argv[1]);
//     serv_addr.sin_family = AF_INET;
//     serv_addr.sin_addr.s_addr = INADDR_ANY;
//     serv_addr.sin_port = htons(portno);
//     if (bind(sockfd, (struct sockaddr *) &serv_addr,
//              sizeof(serv_addr)) < 0)
//              error("ERROR on binding");
//     listen(sockfd,5);
//     clilen = sizeof(cli_addr);
//     newsockfd = accept(sockfd,
//                 (struct sockaddr *) &cli_addr,
//                 &clilen);
//     if (newsockfd < 0)
//          error("ERROR on accept");
//     bzero(buffer,256);
//     n = read(newsockfd,buffer,255);
//     if (n < 0) error("ERROR reading from socket");
//     printf("Here is the message: %s\n",buffer);
//     n = write(newsockfd,"I got your message",18);
//     if (n < 0) error("ERROR writing to socket");
//     close(newsockfd);
//     close(sockfd);
     return 0;
}
