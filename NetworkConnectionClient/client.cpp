#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include <string>
#include <iostream>

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

#define BUFFER_SIZE 5120
class RemoteConsoleClient
{
public:
    RemoteConsoleClient(){

    }
    ~RemoteConsoleClient()
    {
        closeConnection();
    }

    bool connectToServer(const std::string& hostName, const int portNumber)
    {
        _hostName = hostName;
        _portno = portNumber;
        _sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (_sockfd < 0)
        {
            printf("ERROR opening socket\n");
            return false;
        }
        _server = gethostbyname(hostName.c_str());
        if (_server == NULL)
        {
            printf("ERROR, no such host\n");
            return false;
        }
        bzero((char *) &_serv_addr, sizeof(_serv_addr));
        _serv_addr.sin_family = AF_INET;
        bcopy((char *)_server->h_addr,
             (char *)&_serv_addr.sin_addr.s_addr,
             _server->h_length);
        _serv_addr.sin_port = htons(_portno);
        if (connect(_sockfd,(struct sockaddr *) &_serv_addr,sizeof(_serv_addr)) < 0)
        {
            printf("ERROR connecting\n");
            return false;
        }
        return true;
    }

    void closeConnection()
    {
        close(_sockfd);
    }

    bool waitCommand()
    {
        bzero(_buffer,BUFFER_SIZE);
        int n = read(_sockfd,_buffer,BUFFER_SIZE);
        if (n < 0)
        {
             printf("ERROR reading from socket\n");
             return false;
        }

        if(strcmp(_buffer,"exit") == 0) return false;

        // Apply the recived command
        std::string res = exec(_buffer);

        n = write(_sockfd,res.c_str(),res.size());
        if (n < 0)
        {
             printf("ERROR writing to socket\n");
             return false;
        }
        printf("command sent\n");
        return true;
    }

    std::string exec(const char* cmd) {
        FILE* pipe = popen(cmd, "r");
        if (!pipe) return "ERROR";
        char buffer[128];
        std::string result = "";
        while (!feof(pipe)) {
            if (fgets(buffer, 128, pipe) != NULL)
                result += buffer;
        }
        pclose(pipe);
        return result;
    }

private:
    std::string _hostName;
    int _sockfd, _portno;
    struct sockaddr_in _serv_addr;
    struct hostent *_server;
    char _buffer[BUFFER_SIZE];
};

int main(int argc, char *argv[])
{
    RemoteConsoleClient rcc;

    rcc.connectToServer("127.0.0.1", 55555);

    while(1)
    {
        bool res = rcc.waitCommand();
        if(!res) break;
    }

    rcc.closeConnection();

//    int sockfd, portno, n;
//    struct sockaddr_in serv_addr;
//    struct hostent *server;

//    char buffer[256];
//    if (argc < 3) {
//       fprintf(stderr,"usage %s hostname port\n", argv[0]);
//       exit(0);
//    }
//    portno = atoi(argv[2]);
//    sockfd = socket(AF_INET, SOCK_STREAM, 0);
//    if (sockfd < 0)
//        error("ERROR opening socket");
//    server = gethostbyname(argv[1]);
//    if (server == NULL) {
//        fprintf(stderr,"ERROR, no such host\n");
//        exit(0);
//    }
//    bzero((char *) &serv_addr, sizeof(serv_addr));
//    serv_addr.sin_family = AF_INET;
//    bcopy((char *)server->h_addr,
//         (char *)&serv_addr.sin_addr.s_addr,
//         server->h_length);
//    serv_addr.sin_port = htons(portno);
//    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
//        error("ERROR connecting");
//    printf("Please enter the message: ");
//    bzero(buffer,256);
//    fgets(buffer,255,stdin);
//    n = write(sockfd,buffer,strlen(buffer));
//    if (n < 0)
//         error("ERROR writing to socket");
//    bzero(buffer,256);
//    n = read(sockfd,buffer,255);
//    if (n < 0)
//         error("ERROR reading from socket");
//    printf("%s\n",buffer);
//    close(sockfd);
    return 0;
}
