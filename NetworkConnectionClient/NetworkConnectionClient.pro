TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    client.cpp

include(deployment.pri)
qtcAddDeployment()

